// Object - Array conversion
const toArray = obj => {
    return Object.entries(obj);
};

// Sequence array of multiples from desired lenght and starting number
const arrayOfMultiples = (num, length) => {
    return Array.from({ length: length }, (v, i) => num * (i + 1));
};

// Check if coins in wallet are enough (values in amountDue must be converted in right values)
const changeEnough = (change, amountDue) => {
    const values = [0.25, 0.10, 0.05, 0.01];
    return change.map((x, i) => x * values[i]).reduce((acc, cur) => acc + cur) >= amountDue;
};

// Multiple arrays concatenation
const concat = (...arrays) => {
    return arrays.flat();
};

// Black Jack (array assign card value and sum)
const count = cards => {
    return cards && cards.length > 0 ? cards.reduce((a, c) => a + (c <= 6 ? 1 : c <= 9 ? 0 : -1), 0) : 0;
}

// Execution check
console.log(count(["A", "A", "K", "Q", "Q", "J"]));